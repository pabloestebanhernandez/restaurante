﻿using Mod_Ord.bean;
using Mod_Ord.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Mod_Ord.view
{
    /// <summary>
    /// Interaction logic for ContainerTable.xaml
    /// </summary>
    public partial class ContainerTable : UserControl
    {

        private List<EntityCostumer> costumers;


        public ContainerTable( List<EntityCostumer> costumers)
        {
            InitializeComponent();
            this.costumers = costumers;
            //DrawTables(costumers);
        }

        private void DrawTables(List<EntityCostumer> costumers)
        {
            int count = 0;
            int thisColumn = 0;
            int ELEMENT_FOR_COLUMN = 5;
            List<StackPanel> columns = new List<StackPanel>();

            foreach (EntityCostumer c in costumers)
            {
                if (columns.ElementAtOrDefault(count) == null)
                    columns.Add(new StackPanel() { Orientation= Orientation.Vertical ,Margin = new Thickness(10) });
                

                columns[count].Children.Add(Tool.CopyEntityCostumer(c).Table);
                thisColumn++;

                if (thisColumn == ELEMENT_FOR_COLUMN)
                {
                    thisColumn = 0;
                    this.tables.Children.Add(columns[count]);
                    count++;

                }
            }
        }
    }
}
