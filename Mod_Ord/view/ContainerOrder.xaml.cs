﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Mod_Ord.bean;
using Mod_Ord.Utilities;
using Server.Objetos;
using Mod_Ord.utilities;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json;
using System;
using System.Windows.Controls.Primitives;

namespace Mod_Ord.view
{
    /// <summary>
    /// Interaction logic for ContainerOrder.xaml
    /// </summary>
    public partial class ContainerOrder : UserControl
    {
        private List<EntityCostumer> costumers;
        private ConnectionSocket connection;

        public ContainerOrder(List<EntityCostumer> costumers)
        {
            InitializeComponent();

            this.costumers = costumers;
            connection = new ConnectionSocket();
            DrawTables(costumers);
            FillWaiter();
        }


        private void DrawTables(List<EntityCostumer> costumers)
        {
            int count = 0;
            int thisColumn = 0;
            int ELEMENT_FOR_COLUMN = 5;
            List<StackPanel> columns = new List<StackPanel>();  

            foreach (EntityCostumer c in costumers)
            {
                if (columns.ElementAtOrDefault(count) == null)
                    columns.Add(new StackPanel() { Orientation = Orientation.Vertical, Margin = new Thickness(10) });


                c.Table.TOGGLEBUTTON.Click += new RoutedEventHandler(ToggleButton_Click);

                columns[count].Children.Add(Tool.CopyEntityCostumer(c).Table);
                thisColumn++;

                if (thisColumn == ELEMENT_FOR_COLUMN)
                {
                    thisColumn = 0;
                    this.tables.Children.Add(columns[count]);
                    count++;

                }
            }
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if ((sender as ToggleButton).IsChecked == true ? true : false)
            {
                MessageBox.Show("check");
            }
            else
            {
                MessageBox.Show("uncheck");
                // Code for Un-Checked state
            }
        }

        public void FillWaiter() {

           
            dynamic request = new System.Dynamic.ExpandoObject();
            request.tipo = "ObtenerMesero";
            
            string strRequest = JsonConvert.SerializeObject(request);

            string jsonWaiter = connection.Convesation(strRequest);
            List<mesero> waters = JsonConvert.DeserializeObject<List<mesero>>(jsonWaiter);
            cmbWaiter.ItemsSource = waters;
            cmbWaiter.DisplayMemberPath = "nombre";
            cmbWaiter.SelectedValuePath = "codigo";
        
        }

        private void Form_Unloaded(object sender, RoutedEventArgs e)
        {
            //connection.EndConversation();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
