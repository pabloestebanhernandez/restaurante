﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mod_Ord.bean;
using Mod_Ord.Utilities;

namespace Mod_Ord.view
{
    /// <summary>
    /// Interaction logic for ContainerKitchen.xaml
    /// </summary>
    public partial class ContainerKitchen : UserControl
    {
        private List<EntityCostumer> costumers;

        public ContainerKitchen(List<EntityCostumer> costumers)
        {
            InitializeComponent();
            this.costumers = costumers;
            DrawTables(costumers);
        }



        private void DrawTables(List<EntityCostumer> costumers)
        {
            int count = 0;
            int thisColumn = 0;
            int ELEMENT_FOR_COLUMN = 5;
            List<StackPanel> columns = new List<StackPanel>();

            foreach (EntityCostumer c in costumers)
            {
                if (columns.ElementAtOrDefault(count) == null)
                    columns.Add(new StackPanel() { Orientation = Orientation.Vertical, Margin = new Thickness(10) });


                columns[count].Children.Add(Tool.CopyEntityCostumer(c).Table);
                thisColumn++;

                if (thisColumn == ELEMENT_FOR_COLUMN)
                {
                    thisColumn = 0;
                    this.tables.Children.Add(columns[count]);
                    count++;

                }
            }
        }

    }
}
