﻿using Mod_Ord.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.bean
{
    public class EntityCostumer
    {
        public Orden Order { get; set; }
        public string Name { get; set; }
        public Costumer Table { get; set; }

        public EntityCostumer()
        {
            Order = new Orden();
            Table = new Costumer();
        }

    }
}
