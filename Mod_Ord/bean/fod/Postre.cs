﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.bean
{
    public class Postre:Comida
    {


        public static List<Postre> defaultPostres()
        {
            List<Postre> result = new List<Postre>();
            result.Add(new Postre { Nombre = "Pastel", Disponibles = 20, Precio = 50 });
            result.Add(new Postre { Nombre = "Helado", Disponibles = 100, Precio = 90 });
            result.Add(new Postre { Nombre = "Flan", Disponibles = 10, Precio = 80 });
            result.Add(new Postre { Nombre = "Brownie", Disponibles = 10, Precio = 80 });
            return result;
        }

      

    }
}
