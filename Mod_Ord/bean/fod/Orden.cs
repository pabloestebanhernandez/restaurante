﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord
{
    public class Orden
    {
        //Campos
        public List<Platillos> Pedidos; //se refiere a la lista de cosas que pide un cliente.
        public DateTime Fecha_Hora; //Fecha y Cuando se Inicio la Orden.
        public string Titular;
        public double monto; //Monto total de la cuenta.
        public string mesero;
        public string Personas;
        public int y;
        public int x;
        public Orden()
        {

        }

        //Constructor al iniciar cuenta
        public Orden(string name, string mem, string personas){
            this.Pedidos = null;
            this.Fecha_Hora = DateTime.Now;
            this.Titular = name;
            this.mesero = mem;
            this.monto = 0.0;
            this.Personas = personas;
        }
    
    }
}
