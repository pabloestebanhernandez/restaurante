﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.bean
{
    public class Fuerte:Comida
    {


        public static List<Fuerte> defaultFuertes()
        {
            List<Fuerte> result = new List<Fuerte>();
            result.Add(new Fuerte { Nombre = "Pizza", Disponibles = 20, Precio = 50 });
            result.Add(new Fuerte { Nombre = "Hamburgues", Disponibles = 100, Precio = 90 });
            result.Add(new Fuerte { Nombre = "Pollo", Disponibles = 10, Precio = 80 });
            return result;
        }

      

    }
}
