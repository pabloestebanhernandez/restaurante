﻿using Mod_Ord.bean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord
{
   public  class Entrada:Comida
   {


        public static List<Entrada> defaultEntradas()
        {
            List<Entrada> result = new List<Entrada>();
            result.Add( new Entrada { Nombre = "Alitas", Disponibles=20,Precio=50 });
            result.Add( new Entrada { Nombre = "Nachos", Disponibles=100,Precio=30 });
            result.Add( new Entrada { Nombre = "Deditos", Disponibles=10,Precio= 40 });
            result.Add( new Entrada { Nombre = "Sopa", Disponibles=10,Precio= 50 });
            return result;
        }

  
    }
}
