﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Mod_Ord.bean;

namespace Mod_Ord
{
    public class Platillos:Comida
    {
    
        public Platillos(string name, double price, int disp) {
            this.Nombre = name;
            this.Precio = price;
            this.Disponibles = disp;
        }

        public static List<Platillos> get_bebidas()
        {
            List<Platillos> retorno = new List<Platillos>();
            retorno.Add(new Platillos("Coca cola", 12.30, 3));
            retorno.Add(new Platillos("Seven Up", 12.30, 4));
            retorno.Add(new Platillos("Orchata", 5.5, 5));
            retorno.Add(new Platillos("Naranjada Soda", 19.5, 5));
            retorno.Add(new Platillos("Naranjada Agua", 14.5, 5));
            retorno.Add(new Platillos("Grapete", 12.30, 3));
            retorno.Add(new Platillos("Agua Pura", 12.30, 4));
            retorno.Add(new Platillos("Limonada Agua", 5.5, 5));
            retorno.Add(new Platillos("Limonada Soda", 19.5, 5));
            retorno.Add(new Platillos("Mineral", 14.5, 5));
            return retorno;
        }

    }
}
