﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.bean
{
    public class Comida
    {
        public string Nombre { get; set; }
        public double Precio { get; set; }
        public int Disponibles { get; set; }


    }
}
