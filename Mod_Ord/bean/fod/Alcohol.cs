﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.bean
{
    public class Alcohol:Comida
    {


        public static List<Alcohol> defaultLichores()
        {
            List<Alcohol> result = new List<Alcohol>();
            result.Add(new Alcohol { Nombre = "Cerveza", Disponibles = 20, Precio = 50 });
            result.Add(new Alcohol { Nombre = "Tequila", Disponibles = 100, Precio = 90 });
            result.Add(new Alcohol { Nombre = "Whisky", Disponibles = 10, Precio = 80 });
            result.Add(new Alcohol { Nombre = "Vodka", Disponibles = 10, Precio = 80 });
            return result;
        }


    }
}
