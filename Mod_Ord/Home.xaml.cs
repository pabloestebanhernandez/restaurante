﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mod_Ord.utilities;
using Mod_Ord.view;
using Newtonsoft.Json;
using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace Mod_Ord
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : MetroWindow
    {

        private logic.App app;
        private Orden[,] ordenes;
        private ConnectionSocket connection;
        private Orden currentOrder;

        public Home()
        {
            InitializeComponent();
            app = new logic.App();
            ordenes = new Orden[4, 5];
            connection = new ConnectionSocket();

        }

        private async void ShowModalAsync(string v1, string v2)
        {
            await this.ShowMessageAsync(v1, v2);
        }


        private void MetroTabItem_Loaded(object sender, RoutedEventArgs e)
        {
            ShowModalAsync("this is open orders ", "some message");


        }

        private void MetroTabItem_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
        }

        private void MetroTabItem_MouseLeftButtonUp_2(object sender, MouseButtonEventArgs e)
        {
            ShowModalAsync("This is open dbKitchen ", "Some message");
            //dbKitchen.Children.Clear();
            //dbKitchen.Children.Add(app.GetKitchen());
        }

        private void MetroTabItem_MouseLeftButtonUp_3(object sender, MouseButtonEventArgs e)
        {
            ShowModalAsync("This is open gdPay ", "Some message");

        }

        private void MetroTabItem_MouseLeftButtonUp_4(object sender, MouseButtonEventArgs e)
        {
            ShowModalAsync("This is open root ", "Some message");
            gdRoot.Children.Clear();
            gdRoot.Children.Add(new ContainerRoot());
        }



        private void TB_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle = (ToggleButton)sender;
            int y = int.Parse(toggle.Name.Substring(3, 1));
            int x = int.Parse(toggle.Name.Substring(4, 1));
            y--;
            x--;

            bool tableIsFill = ordenes[y, x] == null;
            if (tableIsFill)
            {
                currentOrder = new Orden()
                {
                    Fecha_Hora = DateTime.Now,
                    x = x,
                    y = y
                };
                DrawOrderForm(currentOrder);
                FillWaiter();
            }
            else
            {

                DrawOrderForm(currentOrder);
                cmbWaiter.SelectedItem = currentOrder.mesero;

            }

            //toggle.IsChecked = !tableIsFill;

        }


        private void TB_Clicka(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle = (ToggleButton)sender;
            int y = int.Parse(toggle.Name.Substring(3, 1));
            int x = int.Parse(toggle.Name.Substring(4, 1));
            y--;
            x--;

            bool tableIsFill = ordenes[y, x] == null;
            if (tableIsFill)
            {
                dynamic request = new System.Dynamic.ExpandoObject();
                request.tipo = "ObtenerPlatillo";

                string strRequest = JsonConvert.SerializeObject(request);
                string jsonWaiter = connection.Convesation(strRequest);
                List<Platillo> waters = JsonConvert.DeserializeObject<List<Platillo>>(jsonWaiter);
                //dt.ItemsSource = waters;
            }
            else
            {

            }
        }
        private void DrawOrderForm(Orden currentOrder)
        {
            gdFillOrder.Visibility = Visibility.Visible;

        }

        public void FillWaiter()
        {

            dynamic request = new System.Dynamic.ExpandoObject();
            request.tipo = "ObtenerMesero";

            string strRequest = JsonConvert.SerializeObject(request);
            string jsonWaiter = connection.Convesation(strRequest);
            List<mesero> waters = JsonConvert.DeserializeObject<List<mesero>>(jsonWaiter);
            cmbWaiter.ItemsSource = waters;
            cmbWaiter.DisplayMemberPath = "nombre";
            cmbWaiter.SelectedValuePath = "nombre";
            getPlatillos();

        }

        private void OnClickAdd(object sender, RoutedEventArgs e)
        {
            ShowModalAsync("orden no 1", "Agregada exitosamente");
            currentOrder.mesero = cmbWaiter.SelectedValue.ToString();

            ordenes[currentOrder.y, currentOrder.x] = currentOrder;
            gdFillOrder.Visibility = Visibility.Hidden;


        }
        private void Ordenar_Click(object sender, RoutedEventArgs e)
        {
        }

        private void getPlatillos()
        {
            dynamic request = new System.Dynamic.ExpandoObject();
            request.tipo = "ObtenerPlatillo";
            string strRequest = JsonConvert.SerializeObject(request);
            string jsonPlatillos = connection.Convesation(strRequest);
            List<Platillo> platillos = JsonConvert.DeserializeObject<List<Platillo>>(jsonPlatillos);
           var a =  dt_platillos.ItemsSource;
        }
    }
}