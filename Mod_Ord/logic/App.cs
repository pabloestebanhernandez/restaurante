﻿using Mod_Ord.bean;
using Mod_Ord.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.logic
{
    public class App
    {
        private static App instance= new App();
        List<EntityCostumer> costumers;

        public App()
        {
            costumers = new List<EntityCostumer>();
            for(int a = 0; a < 30; a++)
            {
                EntityCostumer entity = new EntityCostumer();
                costumers.Add(entity);
            }
        }

 
        public ContainerTable GetTables()
        {
            return new ContainerTable(costumers);
        }
        public ContainerOrder GetOrder()
        {
            return new ContainerOrder(costumers);
        }

        public ContainerKitchen GetKitchen()
        {
            return new ContainerKitchen(costumers);
        }
    }
}
