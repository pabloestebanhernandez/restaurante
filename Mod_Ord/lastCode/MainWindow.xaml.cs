﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_Ord
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        //Varable independientes de la clase
        public static int[] Estados_Mesas = new int[25]; //GLOBAL: Estados de la mesas 1=libre, 2=ocupada, 3=por_salir.
        public static Orden[] Ordenes = new Orden[25]; //Inicializamos las 25 ordenes para cada mesa.


        //Control_Mesa frm_controlMesa; //Objeto para ocupar mesa

        public MainWindow()
        {
            InitializeComponent();
        }


        //Evento de caragar el modulo de ordenes
        private void init_order_module(object sender, RoutedEventArgs e)
        {
            //Iniciamos todas las mesas libres (en 1 y color verde).
            for (int i = 1; (i != 26); i++)
            {
                Estados_Mesas[i - 1] = 1;
                Button b = (Button)this.FindName("btn_Mesa" + i.ToString());
                b.Background = Brushes.LightGreen;
            }

            //Inicializamos Control de mesas 

        }


        //Acciones para botones
        private void btn_Mesa2_Click(object sender, RoutedEventArgs e)
        {
            if (Estados_Mesas[1] == 1)
            { //Verificamos si la mesa no esta ocupada.
                Control_Mesa frm_controlMesa = new Control_Mesa();
                frm_controlMesa.No_Mesa = 2;
                frm_controlMesa.Show();
            }
        }

        private void btn_Mesa1_Click(object sender, RoutedEventArgs e)
        {
            if (Estados_Mesas[0] == 1)
            {
                Control_Mesa frm_controlMesa = new Control_Mesa();
                frm_controlMesa.No_Mesa = 1;
                frm_controlMesa.Show();
            }
        }

        private void actualizar_btn_Mesa_2(object sender, MouseEventArgs e)
        {
            if (Estados_Mesas[1] == 2)
            {
                txt_Titular.Text = Ordenes[1].Titular;
                txt_Mesero.Text = Ordenes[1].mesero;
                txt_Personas.Text = Ordenes[1].Personas;
            }
        }

        private void actualizar_btn_Mesa_1(object sender, MouseEventArgs e)
        {
            if (Estados_Mesas[0] == 2)
            {
                txt_Titular.Text = Ordenes[0].Titular;
                txt_Mesero.Text = Ordenes[0].mesero;
                txt_Personas.Text = Ordenes[0].Personas;
            }
        }

        private void btn_Mesa3_Click(object sender, RoutedEventArgs e)
        {
            if (Estados_Mesas[2] == 1)
            { //Verificamos si la mesa no esta ocupada.
                Control_Mesa frm_controlMesa = new Control_Mesa();
                frm_controlMesa.No_Mesa = 3;
                frm_controlMesa.Show();
            }
        }

        private void actualizar_btn_Mesa_3(object sender, MouseEventArgs e)
        {
            if (Estados_Mesas[2] == 2)
            {
                txt_Titular.Text = Ordenes[2].Titular;
                txt_Mesero.Text = Ordenes[2].mesero;
                txt_Personas.Text = Ordenes[2].Personas;
            }
        }




    }
}