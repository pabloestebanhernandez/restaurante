﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mod_Ord
{
    /// <summary>
    /// Lógica de interacción para Control_Mesa.xaml
    /// </summary>
    public partial class Control_Mesa : Window
    {
        //Variables de la clase.
        public int No_Mesa;
        public Control_Mesa()
        {
            InitializeComponent();
        }

        

        private void btn_Cancelar_Click(object sender, RoutedEventArgs e)
        {
            //Cerramos formulario.
            this.Close();
        }

        private void btn_Ocupar_Click(object sender, RoutedEventArgs e)
        {
            int no = this.No_Mesa;
            if (MainWindow.Estados_Mesas[no - 1] == 1) {
                MainWindow.Estados_Mesas[no - 1] = 2; //Pasamos Mesa a Estado Ocupada=2 
                Button b = (Button)(System.Windows.Application.Current.MainWindow).FindName("btn_Mesa" + no.ToString());
                b.Background = Brushes.LightSalmon; //Cambiamos Color de la mesa
                Orden new_ord = new Orden(txt_Titular.Text, cbx_mesero.Text, txt_Persnas.Text); //Creamos Orden
                MainWindow.Ordenes[no - 1] = new_ord; //Almacenamos Ordenes 
                this.Close();
            }
        }

        private void frm_Init_app(object sender, RoutedEventArgs e)
        {
            cbx_mesero.Items.Add("Preng Biba");
            cbx_mesero.Items.Add("Preng Solares");
            cbx_mesero.Items.Add("Biba Solares");
        }
    }
}
