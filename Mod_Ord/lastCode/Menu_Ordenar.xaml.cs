﻿using Mod_Ord.bean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mod_Ord
{
    /// <summary>
    /// Lógica de interacción para Menu_Ordenar.xaml
    /// </summary>
    public partial class Menu_Ordenar : Window
    {
        public Menu_Ordenar()
        {
            InitializeComponent();
        }

        //Evento de Caraga
        private void cargar_items(object sender, RoutedEventArgs e)
        {
            //Cargamos datos.
            dtgrd_Menu_Bebidas.ItemsSource = Platillos.get_bebidas();
            dtgrd_Menu_Entradas.ItemsSource = Entrada.defaultEntradas();
            dtgrd_platoFuerte.ItemsSource = Fuerte.defaultFuertes();
            dtgrd_Menu_Postre.ItemsSource = Postre.defaultPostres();
            dtgrd_Menu_Lichor.ItemsSource = Alcohol.defaultLichores();
        }


        private void AddComida(Comida comida)
        {
            comida.Disponibles = 1;
            dtgrd_Cuenta.Items.Add(comida);
        }

        private void onOrdenarBebida(object sender, RoutedEventArgs e)
        {
            if (Platillos.get_bebidas().Count == 0)
            {
                MessageBox.Show("Are you kidding me...");
            }

            Platillos pl = dtgrd_Menu_Bebidas.SelectedItem as Platillos;

            foreach( Comida a in Platillos.get_bebidas())
            {
                if (a.Nombre.Equals(pl.Nombre))
                {
                    a.Disponibles = a.Disponibles - 1;
                    break;
                }
            }

            AddComida(pl);

        }


        private void onOrdernarEntrada (object sender, RoutedEventArgs e)
        {
            Entrada entrada = (Entrada)dtgrd_Menu_Entradas.SelectedItem;
            AddComida(entrada);
        }



        private void onOrdernarPlatoFuerte(object sender, RoutedEventArgs e)
        {
            Fuerte fuerte = (Fuerte)dtgrd_platoFuerte.SelectedItem;
            AddComida(fuerte);
        }
        private void onOrdernarPostre(object sender, RoutedEventArgs e)
        {
            Postre postre = (Postre)dtgrd_Menu_Postre.SelectedItem;
            AddComida(postre);
        }
        private void onOrdernarLichor(object sender, RoutedEventArgs e)
        {
            Alcohol alcho = (Alcohol)dtgrd_Menu_Lichor.SelectedItem;
            AddComida(alcho);
        }
    }
}
