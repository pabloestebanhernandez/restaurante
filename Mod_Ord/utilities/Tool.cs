﻿using Mod_Ord.bean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod_Ord.Utilities
{
    public static class Tool
    {

        public static EntityCostumer CopyEntityCostumer(EntityCostumer original)
        {
            return new EntityCostumer()
            {
                Name = original.Name,
                Order = new Orden()
                {
                    Fecha_Hora = original.Order.Fecha_Hora,
                    mesero = original.Order.mesero,
                    monto = original.Order.monto,
                    Pedidos = original.Order.Pedidos,
                    Personas = original.Order.Personas,
                    Titular = original.Order.Titular
                },
                Table = new view.Costumer()
               
            };
        }
    }
}
