﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace Mod_Ord.utilities
{
    public class ConnectionSocket
    {
         private static Socket _ClientSocket;
    
        public ConnectionSocket()
        {
            _ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //string ip_jaqui = "172.20.10.6";
            string ip_local = "10.0.75.1";
            int port = 100;
            LoopConnect(ip_local, port);
        }

        public ConnectionSocket(string ip, int port)
        {
            _ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            LoopConnect(ip, port);
        }

        public string Convesation(string request)
        {
            string data = "";
            byte[] SendBuffer;
            byte[] ReceivBuffer = new byte[1024];
            byte[] dataBytes; 
            int ResponseSize = 0;
            SendBuffer = Encoding.Default.GetBytes(request);
            _ClientSocket.Send(SendBuffer); 
            ResponseSize = _ClientSocket.Receive(ReceivBuffer);
            dataBytes = new byte[ResponseSize];
            Array.Copy(ReceivBuffer, dataBytes, ResponseSize); 
            data = Encoding.Default.GetString(dataBytes);
            return data;
        }


        public void EndConversation()
        {
            try
            {
                _ClientSocket.Disconnect(false);
                _ClientSocket.Close();
            }
            catch (Exception)
            {

            }
        }

        private static void LoopConnect(string ConnectionIp, int port)
        {
            IPAddress _ServerIp = IPAddress.Parse(ConnectionIp);
            IPEndPoint _ConnectionInfo = new IPEndPoint(_ServerIp, port);
            int intentos = 0;
            while (!(_ClientSocket.Connected))
            {
                try
                {
                    intentos++;
                    _ClientSocket.Connect(_ConnectionInfo); //Intentamos conexión.
                }
                catch (SocketException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


    }
}
