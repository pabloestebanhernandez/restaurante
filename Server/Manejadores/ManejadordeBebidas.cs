﻿using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Manejadores
{
    class ManejadordeBebidas
    {
        private List<Bebida> bebidas = new List<Bebida>();
        public List<Bebida> obtenerBebidas()
        {
            return bebidas;
        }

        public void inicializarBebidas()
        {
            Bebida n1 = new Bebida() { codigo = 1, nombre = "Naranjada", precio = 20.00, existencia = 20 };
            Bebida n2 = new Bebida() { codigo = 2, nombre = "Cocacola", precio = 25.00, existencia = 50};
            Bebida n3 = new Bebida() { codigo = 3, nombre = "Limonada", precio = 20.00, existencia = 20 };

            bebidas.Add(n1);
            bebidas.Add(n2);
            bebidas.Add(n3);
        }
        public void agregarBebida(Bebida nuevaBebida)
        {
            bebidas.Add(nuevaBebida);

        }
        public void borrarBebida(int codigoBebida)
        {
            foreach (Bebida temporal in bebidas)
            {
                if (temporal.codigo == codigoBebida)
                {
                    bebidas.Remove(temporal);
                }
            }
        }
        public void editarBebida(int codigo, Bebida bebidaEditada)
        {
            foreach (Bebida temporal in bebidas)
            {
                if (temporal.codigo == codigo)
                {
                    temporal.nombre = bebidaEditada.nombre;
                    temporal.precio = bebidaEditada.precio;
                    temporal.existencia = bebidaEditada.existencia;
                }
            }
        }

    }
}
