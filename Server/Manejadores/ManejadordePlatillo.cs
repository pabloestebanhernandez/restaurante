﻿using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Manejadores
{
    public class ManejadordePlatillo
    {
        private List<Platillo> platillos = new List<Platillo>();
        public List<Platillo> obtenerPlatillos()
        {
            return platillos;
        }

        public void inicializarPlatillo()
        {
            Platillo n1 = new Platillo() {codigo = 1, nombre = "hamburguesa", precio = 30.00 };
            Platillo n2 = new Platillo() {codigo = 2, nombre = "pizza", precio = 40.00 };
            Platillo n3 = new Platillo() {codigo = 3, nombre = "spaguetti", precio = 35.00 };
          

            platillos.Add(n1);
            platillos.Add(n2);
            platillos.Add(n3);
           

        }
        public void agregarPlatillo(Platillo nuevoPlatillo)
        {
            platillos.Add(nuevoPlatillo);

        }
        public void borrarPlatillo(int codigoPlatillo)
        {
            foreach (Platillo temporal in platillos)
            {
                if (temporal.codigo == codigoPlatillo)
                {
                    platillos.Remove(temporal);
                }
            }
        }
        public void editarPlatillo(int codigo, Platillo platilloEditado)
        {
            foreach (Platillo temporal in platillos)
            {
                if (temporal.codigo == codigo)
                {
                    temporal.nombre = platilloEditado.nombre;
                    temporal.precio = platilloEditado.precio;
                    temporal.existencia = platilloEditado.existencia;
                }


            }
        }
    }
}
