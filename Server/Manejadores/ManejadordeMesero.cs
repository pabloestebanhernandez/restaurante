﻿using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Manejadores
{
    public class ManejadordeMesero
    {
        private List<mesero> meseros = new List<mesero>();
        public List <mesero> obtenerMeseros()
        {
            return meseros;
        }

        public void inicializarMesero()
        {
            mesero n1 = new mesero() { codigo = 1, nombre = "Juan" };
            mesero n2 = new mesero() { codigo = 2, nombre = "Jose" };
            mesero n3 = new mesero() { codigo = 3, nombre = "Daniel" };
            mesero n4 = new mesero() { codigo = 4, nombre = "Melvin" };

            meseros.Add(n1);
            meseros.Add(n2);
            meseros.Add(n3);
            meseros.Add(n4);

        }
     
        public void agregarMesero( mesero nuevoMesero)
        {
            meseros.Add(nuevoMesero);

        }
        public void borrarMesero(int codigoMesero)
        {
            foreach(mesero temporal in meseros)
            {
                if (temporal.codigo == codigoMesero)
                {
                    meseros.Remove(temporal);
                }
            }
        }
        public void editarMesero(int codigo, mesero meseroEditado)
        {
            foreach(mesero temporal in meseros)
            {
                if (temporal.codigo == codigo)
                {
                    temporal.nombre = meseroEditado.nombre;
                }

            }
        }

    }
}
