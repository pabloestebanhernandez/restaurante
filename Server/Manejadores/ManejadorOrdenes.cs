﻿using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Manejadores
{
    public class ManejadorOrdenes
    {
        List<Orden> ordenes = new List<Orden>();
        public List<Orden> ObtenerOrdenes()
        {
            return ordenes;
        }

        public void inicializarOrdenes()
        {
            ManejadordePlatillo m = new ManejadordePlatillo();
            m.inicializarPlatillo();
            Orden o1 = new Orden() { Name = "Costumer 1 "};
            o1.Platillos.AddRange(m.obtenerPlatillos());
            o1.Total = 80;
            o1.y = 0;
            o1.x = 0;
            ordenes.Add(o1);
            

        }

    }
}
