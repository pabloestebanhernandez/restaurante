﻿using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Manejadores
{
    class ManejadordePostre
    {
        private List<Postre> postres = new List<Postre>();
        public List<Postre> obtenerPostres()
        {
            return postres;
        }

        public void inicializarPostre()
        {
            Postre n1 = new Postre() { codigo = 1, nombre = "helado", precio = 5.00 };
            Postre n2 = new Postre() { codigo = 2, nombre = "pastel", precio = 10.00 };
            Postre n3 = new Postre() { codigo = 3, nombre = "dona", precio = 8.00 };


            postres.Add(n1);
            postres.Add(n2);
            postres.Add(n3);


        }
        public void agregarPostre(Postre nuevoPostre)
        {
            postres.Add(nuevoPostre);

        }
        public void borrarPostre(int codigoPostre)
        {
            foreach (Postre temporal in postres)
            {
                if (temporal.codigo == codigoPostre)
                {
                    postres.Remove(temporal);
                }
            }
        }
        public void editarPostre(int codigo, Postre postreEditado)
        {
            foreach (Postre temporal in postres)
            {
                if (temporal.codigo == codigo)
                {
                    temporal.nombre = postreEditado.nombre;
                    temporal.precio = postreEditado.precio;
                    temporal.existencia = postreEditado.existencia;
                }


            }
        }
    }
}
