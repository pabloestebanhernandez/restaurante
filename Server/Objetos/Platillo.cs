﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Objetos
{
    public class Platillo
    {
        public string nombre { get; set; }
        public double precio { get; set;}
        public int codigo { get; set;}
        public int existencia { get; set;}
    }
}
