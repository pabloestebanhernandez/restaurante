﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Objetos
{
    public class Orden
    {
        internal int y;
        internal int x;

        public string Name { get; set; }
        public int Total { get; set; }
    
        public List<Platillo> Platillos { get; set; }

    }
}
