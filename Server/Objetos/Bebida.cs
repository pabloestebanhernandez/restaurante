﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Objetos
{
    class Bebida
    {
            public string nombre { get; set; }
            public double precio { get; set; }
            public int existencia { get; set; }
            public int codigo { get; set; }
    }
}
