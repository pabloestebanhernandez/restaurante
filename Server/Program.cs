﻿using Newtonsoft.Json;
using Server.Manejadores;
using Server.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace Server
{
    class Program
    {
       
        private static byte[] _buffer = new byte[1024]; //Buffer para entrada de datos.
        private static List<Socket> _clientSockets = new List<Socket>(); //Lista de sockets para maenjo de respuestas.
        private static Socket _ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        //Main
        static void Main(string[] args)
        {
            System.Console.Title = "Servidor Multicliente";
            initServer(100);
            System.Console.ReadLine();
        }




        //Duncion para iniciar el servidor.
        private static void initServer(int _port)
        {
            System.Console.WriteLine("Ininiando Servidor...");
            IPEndPoint _ServerInfo = new IPEndPoint(IPAddress.Any, _port); //Información de la conexión.
            _ServerSocket.Bind(_ServerInfo); //Enlace.

            System.Console.WriteLine("Esperando Conexiones...");
            _ServerSocket.Listen(5); //Iniciamos a escuchar.

            _ServerSocket.BeginAccept(new AsyncCallback(AcceptCallback), null); //Operacion para aceptar conexiones asincronas.
        }


        //Accion para aceptar las conexiones.
        private static void AcceptCallback(IAsyncResult _AsyncState)
        {
            Socket _ResponseSocket = _ServerSocket.EndAccept(_AsyncState);
            _clientSockets.Add(_ResponseSocket);
            System.Console.WriteLine("Cliente Conectado...");
            _ResponseSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), _ResponseSocket); //Acciones para recibir data.
            _ServerSocket.BeginAccept(new AsyncCallback(AcceptCallback), null); //Permitimos aceptar mas conexiones.
        }

        //Accion para la información recibida.
        private static void ReceiveCallback(IAsyncResult _AsyncState)
        {
            try
            {
                Socket _ResponseSocket = (Socket)_AsyncState.AsyncState; //Parametro enviado por el Callback.
                int received = _ResponseSocket.EndReceive(_AsyncState);
                byte[] data = new byte[received]; //Información captada.
                Array.Copy(_buffer, data, received);
                string text = Encoding.Default.GetString(data);
                Console.WriteLine("Datos Recibidos: " + text);
                string response = ejecutarPeticion(text);
                byte[] time = Encoding.Default.GetBytes(response);
                _ResponseSocket.BeginSend(time, 0, time.Length, SocketFlags.None, new AsyncCallback(SendCallback), _ResponseSocket);
                _ResponseSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), _ResponseSocket); //Acciones para recibir data.
                _ServerSocket.BeginAccept(new AsyncCallback(AcceptCallback), null); //Permitimos aceptar mas conexiones.
            }
            catch (Exception)
            {
                Console.WriteLine("Cliente desconectado");
            }
        }


        private static void SendCallback(IAsyncResult _AsyncState)
        {
            Socket _SendSocket = (Socket)_AsyncState.AsyncState;
            _SendSocket.EndSend(_AsyncState);

        }


        private static string ejecutarPeticion(string text)
        {
            try
            {
                dynamic peticion = JsonConvert.DeserializeObject<dynamic>(text);

                string opcion = peticion.tipo;


                switch (opcion)
                {

                    case "ObtenerMesero":

                        return caseMeseros();

                    case "AgregarMesero":

                        return caseAgregarMesero(peticion.Mesero);

                    case "EditarMesero":

                        return caseEditarMesero(peticion.codigo, peticion.nombre);

                    case "BorrarMesero":

                        return caseBorrarMesero(peticion.codigo);

                    case "ObtenerPlatillo":

                        return casePlatillo();

                    case "AgregarPlatillo":

                        return caseAgregarPlatillo(peticion.platillo);

                    case "EditarPlatillo":

                        return caseEditarPlatillo(peticion.codigo, peticion.platillo);

                    case "BorrarPlatillo":

                        return caseBorrarPlatillo(peticion.codigo);

                    case "ObtenerBebida":

                        return caseBebida();

                    case "AgregarBebida":

                        return caseAgregarBebida(peticion.bebida);

                    case "EditarBebida":

                        return caseEditarBebida(peticion.codigo, peticion.bebida);

                    case "BorrarBebida":

                        return caseBorrarBebida(peticion.codigo);

                    case "ObtenerPostre":

                        return casePostre();

                    case "AgregarPostre":

                        return caseAgregarPostre(peticion.postre);

                    case "EditarProstre":

                        return caseEditarPostre(peticion.codigo, peticion.postre);

                    case "MostrarOrdenes":
                        return caseAgregarOrden();
                    
                    
                    case "BorrarPostre":

                        return caseBorrarPostre(peticion.codigo);
                    default:
                        return "not found";

                }
            }
            catch (Exception e) {
                Console.Write(e.Message);
                return e.Message;
            }
        }

        private static string caseAgregarOrden()
        {
            List<Orden> temporalpl = new List<Orden>();
            ManejadorOrdenes manejador = new ManejadorOrdenes();

            manejador.inicializarOrdenes();
            temporalpl = manejador.ObtenerOrdenes();

            string json = JsonConvert.SerializeObject(temporalpl);
            return json;
        }

        private static string caseEditarPostre(dynamic codigo, dynamic postre)
        {
            int codigoPostre = (int)codigo;
            Postre nuevoPostre = (Postre)postre;
            ManejadordePostre manejador = new ManejadordePostre();
            manejador.editarPostre(codigoPostre, nuevoPostre);
            return "Postre editado exitosamente";
        }

        private static string caseBorrarPostre(dynamic codigo)
        {
            int borrarPostre = (int)codigo;
            ManejadordePostre manejador = new ManejadordePostre();
            manejador.borrarPostre(borrarPostre);
            return "Postre eliminado exitosamente";
        }

        private static string caseAgregarPostre(dynamic postre)
        {
            Postre nuevaPostre = (Postre)postre;
            ManejadordePostre manejador = new ManejadordePostre();
            manejador.agregarPostre(nuevaPostre);
            return "Postre agregado exitosamente";
        }

        private static string casePostre()
        {
            List<Postre> temporalpl = new List<Postre>();
            ManejadordePostre manejador = new ManejadordePostre();

            manejador.inicializarPostre();
            temporalpl = manejador.obtenerPostres();

            string json = JsonConvert.SerializeObject(temporalpl);

            return json;
        }

        private static string caseBorrarBebida(dynamic codigo)
        {
            int borrarBebida = (int)codigo;
            ManejadordeBebidas manejador = new ManejadordeBebidas();
            manejador.borrarBebida(borrarBebida);
            return "Bebida eliminada exitosamente";
        }

        private static string caseEditarBebida(dynamic codigo, dynamic bebida)
        {
            int codigoBebida = (int)codigo;
            Bebida nuevaBebida = (Bebida)bebida;
            ManejadordeBebidas manejador = new ManejadordeBebidas();
            manejador.editarBebida(codigoBebida, nuevaBebida);
            return "Bebida editada";
        }

        private static string caseAgregarBebida(dynamic bebida)
        {
            Bebida nuevaBebida = (Bebida)bebida;
            ManejadordeBebidas manejador = new ManejadordeBebidas();
            manejador.agregarBebida(nuevaBebida);
            return "Bebida agregada exitosamente";
        }

        private static string caseBebida()
        {
            List<Bebida> temporalpl = new List<Bebida>();
            ManejadordeBebidas manejador = new ManejadordeBebidas();

            manejador.inicializarBebidas();
            temporalpl = manejador.obtenerBebidas();

            string json = JsonConvert.SerializeObject(temporalpl);

            return json;
        }

        private static string caseAgregarMesero(dynamic Mesero)
        {
            mesero nuevoMesero = (mesero)Mesero;
            ManejadordeMesero manejador = new ManejadordeMesero();
            manejador.agregarMesero(nuevoMesero);
            return "Mesero agregado exitosamente";

        }

        private static string caseEditarMesero(dynamic codigo, dynamic nombre)
        {
            int codigoMesero = (int)codigo;
            mesero nuevoMesero = (mesero)nombre;
            ManejadordeMesero manejador = new ManejadordeMesero();
            manejador.editarMesero(codigoMesero, nuevoMesero);
            return "Mesero editado";
        }

        private static string caseBorrarMesero(dynamic codigo)
        {
            int borrarMesero = (int)codigo;
            ManejadordeMesero manejador = new ManejadordeMesero();
            manejador.borrarMesero(borrarMesero);
            return "Mesero eliminado exitosamente";
           
        }

        private static string caseBorrarPlatillo(dynamic codigo)
        {
            int borrarPlatillo = (int)codigo;
            ManejadordePlatillo manejador = new ManejadordePlatillo();
            manejador.borrarPlatillo(borrarPlatillo);
            return "Platillo eliminado exitosamente";
        }

        private static string caseEditarPlatillo(dynamic codigo, dynamic platillo)
        {
            int codigoPlatillo = (int)codigo;
            Platillo nuevoPlatillo = (Platillo)platillo;
            ManejadordePlatillo manejador = new ManejadordePlatillo();
            manejador.editarPlatillo(codigoPlatillo, nuevoPlatillo);
            return "Platillo editado";
        }

        private static string caseAgregarPlatillo(dynamic platillo)
        {
            Platillo nuevoPlatillo = (Platillo)platillo;
            ManejadordePlatillo manejador = new ManejadordePlatillo();
            manejador.agregarPlatillo(nuevoPlatillo);
            return "Platillo agregado exitosamente";
        }

        private static string casePlatillo()
        {
            List<Platillo> temporalpl = new List<Platillo>();
            ManejadordePlatillo manejador = new ManejadordePlatillo();

            manejador.inicializarPlatillo();
            temporalpl = manejador.obtenerPlatillos();

            string json = JsonConvert.SerializeObject(temporalpl);

            return json;

        }

        private static string caseMeseros()
        {
            List<mesero> temporal = new List<mesero>();
            ManejadordeMesero manejador = new ManejadordeMesero();

            manejador.inicializarMesero();
            temporal = manejador.obtenerMeseros();

            string json = JsonConvert.SerializeObject(temporal);

            return json;
        }
    }
}
